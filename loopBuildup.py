import re
import sys

R=0
W=1
SCA=0
ARR=1

mesh_decompose=True

replace_divs = True

include_ifdefs = True # establishes if the variables betwen #ifdef #endif should be included

scalar_vars={} #vars you write to are 1, all others read only
array_vars={} #vars you write to are 1, all others read only
converted_vars={} 

def_vars={}
div_vars={}

def isVar(s):
    return re.match(r"^[A-Za-z]+\w*",s)

def isArray(s):
    return re.match(r"[A-Za-z]+\w*\(.+\)",s)

def hasConversion(s):
    return re.match("NM[I]?\d",s.upper())

def loadCode(filename):
    global include_ifdefs
    global def_vars
    
    f = open(filename, "rt")
    code=[] 
    remove=0
    for line in f:
        if line.startswith("C"):
            continue
        if line.startswith("#ifdef"):
            m=re.match("#ifdef\s+(?P<var>[^\s]+)",line)
            def_vars[m.group("var")]=1
            remove+=1
            continue
        if line.startswith("#endif"):
            remove-=1
            continue
        if remove>0:
            if not include_ifdefs:
                continue
        line=line.strip()
        lcode=line.split("!")[0]
        lcode=lcode.rstrip()
        if len(lcode)!=0:
            if lcode.startswith("&"):
                lcode=lcode.replace("&"," ")
                lcode=lcode.strip()
                code[-1]=code[-1]+lcode
            else:
                code.append(lcode)
    return code

def saveVariable(name,typ,rw,dim=1,real=1):
    global scalar_vars
    global array_vars
    name=name.upper()
    if typ==0:  # scalar    
        if name in scalar_vars:
            if rw==1 or real==0:
                scalar_vars[name]=(rw,real)
        else:
            scalar_vars[name]=(rw,real)
    else:   # array
        if name in array_vars:
            if rw==1:
                array_vars[name]=(rw,dim)
        else:
            array_vars[name]=(rw,dim)

def saveArrayVar(var,idxs,RW):
    global converted_vars
    global mesh_decompose
    convert=None
    for idx in idxs:                    
        if not idx.isdigit():
            if mesh_decompose and hasConversion(idx):
                saveVariable("NE",SCA,R,real=0)
                convert=idx
            else:
                saveVariable(idx,SCA,R,real=0)
    if mesh_decompose and convert is not None:
        saveVariable(var+convert+"_VEC",ARR,RW,dim=len(idxs))
        converted_vars[var.upper()]=var.upper()+convert.upper()+"_VEC"        
    else:
        saveVariable(var,ARR,RW,dim=len(idxs))


def parseExpression(expr):
    global R,W,ARR,SCA

    opcount=0

    if expr[0]=='(':
        subexpr=""
        pos=1
        openp=1
        while openp>0:
            if expr[pos]=='(':
                openp+=1
            elif expr[pos]==')':
                openp-=1
            if openp>0:
                subexpr+=expr[pos]
            pos+=1 
        opcount+=parseExpression(subexpr)
        if pos<len(expr):
            if expr[pos] in ['*','+','-','/']:
                opcount+=1
                pos+=1
                opcount+=parseExpression(expr[pos:])
            else:
                m=re.match(r"^(?P<op>\>=|\<=|==|/=|\.EQ\.|\.LT\.|\.LE\.|\.GE\.|\.GT\.|\.AND\.|\.OR\.|\.NOT\.|\.EQV\.|\.NEQV\.|\>|\<|[\*\+\-/])(?P<rest>.*)",expr[pos:])
                if m:
                    if m.group("rest"):
                        opcount+=parseExpression(m.group("rest"))
    else:
        m=re.match(r"(?P<sign>-)?(?P<var>[A-Z_]+\w*|\d+(\.\d*)?([DE][\+\-]?\d+)?)(?P<idx>\([^\)]+\))?(?P<op>\>=|\<=|==|/=|\.EQ\.|\.LT\.|\.LE\.|\.GE\.|\.GT\.|\.AND\.|\.OR\.|\.NOT\.|\.EQV\.|\.NEQV\.|\>|\<|[\*\+\-/])?(?P<rest>.*)",expr.upper())
        if re.match(r"\d",m.group("var")[0]):
            pass
        else:       
            if m.group("idx"):  # array                
                idxs=m.group("idx")[1:-1].split(",")
                saveArrayVar(m.group("var"),idxs,R)
            else:
                saveVariable(m.group("var"),SCA,R)
        if m.group("sign") is not None:
            opcount+=1
        if m.group("op") is not None:
            if m.group("op") in ['*','+','-','/']:
                opcount+=1
            opcount+=parseExpression(m.group("rest"))
    return opcount
    
def parseVariables(code):
    global R,W,ARR,SCA
    fortran_keyword = ["IF","DO","END","ENDDO","ENDIF", "CONTINUE"]
    enddo=None
    loop=[]
    opendos=0
    openifs=0
    for c in code:

        # BUG WORK AROUND
        while " .OR." in c.upper():
            c = c.replace(" .OR.", ".OR.")
        # END OF BUG WORK AROUND

        found=False
        keys=[[k,c.upper().find(k)] for k in fortran_keyword if c.upper().find(k)!=-1]
        if len(keys)>0:
            sz=keys[0][1]+len(keys[0][0])
            if sz<len(c):
                if c[sz].isspace() or not c[sz].isalnum():
                    found=True
            else:
                found=True
            if found:
                keys.sort(key=lambda x: x[1])
                if keys[0][0]=="DO":
                    m=re.match("DO (?P<line>\d+)?\s*(?P<var>[A-Za-z]+\w*)\s*=\s*(?P<start>[A-Za-z\d_]+),(?P<end>[A-Za-z\d_]+)",c.upper())
                    if m:
                        opendos+=1
                        enddo=m.group("line")
                        if isVar(m.group("var")):
                            saveVariable(m.group("var"),SCA,R,real=0)
                        if isVar(m.group("start")):
                            saveVariable(m.group("start"),SCA,R,real=0)
                        if isVar(m.group("end")): 
                            saveVariable(m.group("end"),SCA,R,real=0)
                        nuloop="DO "+m.group("var").upper()+"="+m.group("start").upper()+","+m.group("end").upper()
                        loop.append([c,nuloop,m.group("var").upper()])
                elif keys[0][0]=="END":
                    m=re.match("END\s*DO",c)
                    if m:                    
                        opendos-=1
                        lst=loop[opendos]
                        lst.append(c)
                        loop[opendos]=lst
                    else:
                        m=re.match("END\s*IF",c)
                        if m:
                            openifs-=1                    
                elif keys[0][0]=="CONTINUE":
                    if enddo is not None and c.startswith(enddo):
                        opendos-=1
                        lst=loop[opendos]
                        lst.append(c)
                        loop[opendos]=lst
                elif keys[0][0]=="IF":
                    m=re.match("IF\s*(?P<condition>[^\s]+)\s*(THEN)?\s*(?P<expr>[^\s]+)?",c.upper())
                    if m:
                        openifs+=1
                        parseExpression(m.group("condition"))
                        if m.group("expr"):
                            pos=m.group("expr").find("=")
                            if pos!=-1:
                                expr=[m.group("expr")[:pos],m.group("expr")[pos+1:]]
                                if isArray(expr[0]):           
                                    m=re.match(r"(?P<var>[A-Za-z]+\w*)\((?P<idx>.+)\)",expr[0])               
                                    idxs=m.group("idx").split(",")
                                    saveArrayVar(m.group("var"),idxs,W)
                                else:
                                    saveVariable(expr[0],SCA,W)
                                parseExpression(expr[1])
                            else:
                                parseExpression(m.group("expr"))
                elif keys[0][0]=="ENDIF":
                    openifs-=1
        if not found:
            c=c.replace(" ","")
            pos=c.find("=")
            if pos!=-1:
                expr=[c[:pos],c[pos+1:]] #c.split("=")
                if isArray(expr[0]):           
                    m=re.match(r"(?P<var>[A-Za-z]+\w*)\((?P<idx>.+)\)",expr[0])               
                    idxs=m.group("idx").split(",")
                    saveArrayVar(m.group("var"),idxs,W)
                else:
                    saveVariable(expr[0],SCA,W)
                parseExpression(expr[1])
            else:
                parseExpression(c)
    return loop

def countOperations(code):
    fortran_keyword = ["IF","DO","END","ENDDO","ENDIF", "CONTINUE"]
    count=0
    for c in code:

        # BUG WORK AROUND
        while " .OR." in c.upper():
            c = c.replace(" .OR.", ".OR.")
        # END OF BUG WORK AROUND

        found=False
        keys=[[k,c.upper().find(k)] for k in fortran_keyword if c.upper().find(k)!=-1]
        if len(keys)>0:
            sz=keys[0][1]+len(keys[0][0])
            if sz<len(c):
                if c[sz].isspace() or not c[sz].isalnum():
                    found=True
            else:
                found=True
            if found:
                keys.sort(key=lambda x: x[1])
                if keys[0][0]=="IF":
                    m=re.match("IF\s*(?P<condition>[^\s]+)\s*(THEN)?\s*(?P<expr>[^\s]+)?",c.upper())
                    if m:
                        count+=parseExpression(m.group("condition"))
                        if m.group("expr"):
                            pos=m.group("expr").find("=")
                            if pos!=-1:
                                expr=[m.group("expr")[:pos],m.group("expr")[pos+1:]]
                                count+=parseExpression(expr[1])
                            else:
                                count+=parseExpression(m.group("expr"))
        if not found:  
            c=c.replace(" ","")
            pos=c.find("=")
            if pos!=-1:
                expr=[c[:pos],c[pos+1:]]
                count+=parseExpression(expr[1])
            else:
                count+=parseExpression(c)
    return count

def countLoads(code):
    fortran_keyword = ["IF","DO","END","ENDDO","ENDIF", "CONTINUE"]
    count=0
    for c in code:
        found=False
        keys=[[k,c.upper().find(k)] for k in fortran_keyword if c.upper().find(k)!=-1]
        if len(keys)>0:
            sz=keys[0][1]+len(keys[0][0])
            if sz<len(c):
                if c[sz].isspace() or not c[sz].isalnum():
                    found=True
            else:
                found=True
            if found:
                keys.sort(key=lambda x: x[1])
                if keys[0][0]=="IF":
                    m=re.match("IF\s*(?P<condition>[^\s]+)\s*(THEN)?\s*(?P<expr>[^\s]+)?",c.upper())
                    if m:
                        if m.group("expr"):
                            if c.find("=")!=-1:
                                count+=1
        if not found:  
            if c.find("=")!=-1:
                count+=1
    return count
    
def get_var_list(string):
    remove_list = [' ', '&', '\n', '\t']
    operators = ['+', '-', '*', '/']
    separator = "<>"
    lhs_var = None

    string = string.split('!')[0]  # removes comments from the end of the string

    # removes unnecessary symbols
    for remove_symbol in remove_list:
        string = string.replace(remove_symbol, '')

    if '=' in string:
        split = string.split('=')
        lhs_var = split[0]  # left hand side
        string = split[1]  # right hand side

    for operator in operators:  # replaces all operators with separator
        string = string.replace(operator, separator)

    var_list = string.split(separator)
    var_list = filter(None, var_list)  # removes all empty strings in the list. Note, wont work in python ver >= 3

    # removes  unnecessary bracket from var string names
    for i in range(0, len(var_list)):

        while var_list[i].startswith('('):
            var_list[i] = var_list[i].replace('(', '')

        while var_list[i].endswith("))"):
            var_list[i] = var_list[i].replace("))", ")")

    # filter removes all empty strings in the list. Note, wont work in python ver > 3
    # returns tuple. All variables on RHS and assignment var if one exist
    return [filter(None, var_list), lhs_var]


def get_memory_loads(lines):
    ignore_list = ["#", "!", "END", "CONTINUE"]

    loaded_arrays = []
    loaded_scalars = []

    counter = None
    memory_load_count = 0

    for line in lines:

        if line.upper().startswith('C'): # ignores comments
            continue

        line = line.lstrip()  # removes start/end white spaces/new lines etc..

        # extracts counter so it would be ignored
        if counter is None and line.replace(' ', '').upper().startswith("DO"):
            split_list = line.split('=')[0].split(' ')
            split_list = filter(None, split_list)
            counter = split_list[len(split_list) - 1]
            continue

        # ignores comments and def's and empty strings
        ignore = False
        for keyWord in ignore_list:
            if not line or line.upper().startswith(keyWord):
                ignore = True
        if ignore:
            continue

        result = get_var_list(line)

        # processes RHS
        var_list = result[0]
        for var in var_list:

            # ignores hardcoded digits and counter
            if var[0].isdigit():
                continue

            # checks arrays for memory loads
            if '(' in var and ')' in var:
                indexes = var[var.find("(")+1:var.find(")")]  # gets array indexes
                var = var.split('(')[0]  # gets array name

                if var not in loaded_arrays:
                    memory_load_count += 1
                    loaded_arrays.append(var)

                index_list = indexes.replace(' ', '').split(',')  # in case its multi dimensional array

                for index in index_list:

                    # covers case like this: Coef(NMI3,J32_VEC(IE))
                    # works only if the index of the array(which is used as an index for main array) is scalar
                    if '(' in index:
                        split = index.split('(')

                        if split[0] not in loaded_arrays:
                            memory_load_count += 1
                            loaded_arrays.append(split[0])

                        if split[1] != counter and split[1] not in loaded_scalars and not split[1][0].isdigit():
                            memory_load_count += 1
                            loaded_scalars.append(split[1])
                        continue

                    if index != counter and index not in loaded_scalars and not index[0].isdigit():
                        memory_load_count += 1
                        loaded_scalars.append(index)

            else:  # checks scalars for memory loads

                while var.endswith(')'):  # removes any extra brackets if some where missed when processing
                    var = var.replace(')', '')

                if var != counter and var not in loaded_scalars:
                    memory_load_count += 1
                    loaded_scalars.append(var)

        # processes LHS
        if result[1]:

            lhs_var = result[1]
            if '(' in lhs_var and ')' in lhs_var:
                lhs_indexes = lhs_var[lhs_var.find("(")+1:lhs_var.find(")")]  # gets array indexes
                lhs_var = lhs_var.split('(')[0]  # gets array name

                if lhs_var not in loaded_arrays:
                    loaded_arrays.append(var)

                lhs_index_list = lhs_indexes.replace(' ', '').split(',')  # in case its multi dimensional array

                for lhs_index in lhs_index_list:

                    # covers case like this: Coef(NMI3,J32_VEC(IE))
                    # works only if the index of the array(which is used as an index for main array) is scalar
                    if '(' in lhs_index:
                        lhs_split = lhs_index.split('(')

                        if lhs_split[0] not in loaded_arrays and not lhs_split[1][0].isdigit():
                            memory_load_count += 1
                            loaded_arrays.append(lhs_split[0])

                        if lhs_split[1] != counter and lhs_split[1] not in loaded_scalars:
                            memory_load_count += 1
                            loaded_scalars.append(lhs_split[1])
                        continue

                    if lhs_index != counter and lhs_index not in loaded_scalars and not lhs_index[0].isdigit():
                        memory_load_count += 1
                        loaded_scalars.append(lhs_index)

            else:  # checks scalars for memory loads
                if lhs_var not in loaded_scalars:
                    loaded_scalars.append(lhs_var)

    #print("Number of memory loads: %s" % memory_load_count)
    return memory_load_count



if len(sys.argv) < 2:
    print "This program takes a simple text file as an argument."
    print "The file must begin with a DO IE=1,NE and end with either CONTINUE or END DO"
    print "usage: python "+sys.argv[0]+" /path/to/the/input.txt"
    exit()

code=loadCode(sys.argv[1])

loop=parseVariables(code)

if loop[0][2] in scalar_vars:
    del scalar_vars[loop[0][2]]
if "NE" in scalar_vars:
    del scalar_vars['NE']
if mesh_decompose:
    pass
#    if "NM1" in scalar_vars:
#        del scalar_vars['NM1']
#    if "NM2" in scalar_vars:
#        del scalar_vars['NM2']
#    if "NM3" in scalar_vars:
#        del scalar_vars['NM3']
#    if "NMI1" in scalar_vars:
#        del scalar_vars['NMI1']
#    if "NMI2" in scalar_vars:
#        del scalar_vars['NMI2']
#    if "NMI3" in scalar_vars:
#        del scalar_vars['NMI3']

outFile = open("loop.F90",'w')
f = open(sys.argv[1], "r")
lines=[]
for line in f:
    lines.append(line)
lastcode=0
for i,line in enumerate(lines):
    if line.startswith("C"):
        lines[i]='!'+line[1:]       # for making it work with gfortran
    elif line.startswith("#ifdef"):
        m=re.match("#ifdef\s+(?P<var>[^\s]+)",line)
        lines[i]="#if "+m.group("var")+"\n"
    elif line.find(loop[0][0])!=-1:
        lines[i]="    "+loop[0][1]+"\n"
        lastcode=i
    elif line.find(loop[0][3])!=-1:
        lines[i]="    END DO"
        lastcode=i
    elif line.find('&')!=-1:        # for making it work with gfortran
        lines[lastcode]=lines[lastcode].replace("\n"," &\n")
        lines[i]=line
        lastcode=i
    else:
        lines[i]=line
        if len(line.strip())>0:
            lastcode=i
    if mesh_decompose:
        if re.search("[Nn][Mm]([Ii])?\d\s*=",lines[i]):
            #line=""
            pass
        else:
            line=re.sub("\([Nn][Mm]([Ii])?\d","(IE",lines[i])
            for c in converted_vars:
                m=re.search("[^\w]?"+c+"\(",line.upper())
                if m:
                    line=line.upper().replace(c+"(",converted_vars[c]+"(")
        lines[i]=line
    if replace_divs:
        lst=re.findall("/(?P<dec>\d+)(?P<frac>\.\d*)?(?P<exp>[dDeE][\+-]?\d+)?",lines[i])
        if len(lst)>0:
            for dec,frac,exp in lst:
                if float("0"+frac+exp.upper().replace('D','E'))==0.0:
                    varname="divby"+dec
                    div_vars[varname]=dec+frac+exp
                    saveVariable(varname,SCA,W)
                    lines[i]=lines[i].replace("/"+dec+frac+exp,"*"+varname)

fileBody=[]
fileBody.append("PROGRAM test")
if len(def_vars)>0:
    fileBody.append("#define TRUE  1")
    fileBody.append("#define FALSE 0")
    for var in def_vars:
        fileBody.append("#define "+var+" FALSE")
fileBody.append("USE OMP_LIB")
fileBody.append("IMPLICIT NONE")
fileBody.append('INTEGER,PARAMETER :: NE=1000')
fileBody.append('INTEGER,PARAMETER :: SZ=8')

fileBody.append('INTEGER :: '+loop[0][2])

for var in array_vars:
    item=array_vars[var]
    vdef='REAL(SZ),ALLOCATABLE :: '+var+"("
    vdef+=",".join([":" for i in range(0,item[1])])
    vdef+=")"
    fileBody.append(vdef)

for var in scalar_vars:
    item=scalar_vars[var]
    if item[1]==0:  # integer
        fileBody.append('INTEGER :: '+var)
    else:   # real
        fileBody.append('REAL(SZ) :: '+var)

fileBody.append('double precision t1, t2')

for var in array_vars:
    item=array_vars[var]
    vdef='    ALLOCATE('+var+"("
    vdef+=",".join(["1:NE" for i in range(0,item[1])])
    vdef+="))"
    fileBody.append(vdef)

if replace_divs:            
    for var in div_vars:
        fileBody.append('    '+var+" = 1/"+div_vars[var])
          
outFile.write("\n".join(fileBody))
outFile.write('\n    t1 = omp_get_wtime()\n\n')
outFile.write("".join(lines))
outFile.write('\n    t2 = omp_get_wtime()\n')
outFile.write("    print*,'Execution time = ', t2-t1\n")
outFile.write("    print*,'GFLOPs achieved = ', (NE*"+str(get_memory_loads(code))+"/1.0d9/(t2-t1))\n\n")
outFile.write('END PROGRAM\n')


print "Number of operations:",countOperations(code)
print "Number of loads:",get_memory_loads(code)

