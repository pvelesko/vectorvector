# usage just import get_memory_loads function. Returns int
# Global vars
loaded_arrays = []
loaded_scalars = []
memory_load_count = 0
counter = None

# pre processes string
import sys
# returns tuple. All variables on RHS and LHS var if one exist
def _get_var_list(string):
    remove_list = [' ', '&', '\n', '\t']
    operators = ['+', '-', '*', '/']
    separator = "<>"
    lhs_var = None

    string = string.split('!')[0]  # removes comments from the end of the string

    # removes unnecessary symbols
    for remove_symbol in remove_list:
        string = string.replace(remove_symbol, '')

    if '=' in string:
        split = string.split('=')
        lhs_var = split[0]  # left hand side
        string = split[1]  # right hand side

    for operator in operators:  # replaces all operators with separator
        string = string.replace(operator, separator)

    var_list = string.split(separator)

    # removes  unnecessary bracket from var string names
    for i in range(0, len(var_list)):

        while var_list[i].startswith('('):
            var_list[i] = var_list[i].replace('(', '')

        while var_list[i].endswith("))"):
            var_list[i] = var_list[i].replace("))", ")")

    # filter removes all empty strings in the list. Note, wont work in python ver > 3
    return [filter(None, var_list), lhs_var]


# Processes and parses indexes of arrays
def _process_indexes(index_list):
    global memory_load_count, counter, loaded_arrays, loaded_scalars

    for index in index_list:

        # covers case like this: Coef(NMI3,J32_VEC(IE))
        # works only if the index of the array(which is used as an index for main array) is scalar
        if '(' in index:
            split = index.split('(')

            if split[0] not in loaded_arrays and not split[1][0].isdigit():
                memory_load_count += 1
                loaded_arrays.append(split[0])

            if split[1] != counter and split[1] not in loaded_scalars:
                memory_load_count += 1
                loaded_scalars.append(split[1])
            continue

        if index != counter and index not in loaded_scalars and not index[0].isdigit():
            memory_load_count += 1
            loaded_scalars.append(index)


def get_memory_loads(lines):
    global memory_load_count, counter, loaded_arrays, loaded_scalars

    ignore_list = ["#", "!", "END", "CONTINUE", "IF", "THEN"]

    for line in lines:

        if line.upper().startswith('C'):  # ignores comments
            continue

        line = line.lstrip()  # removes start/end white spaces/new lines etc..

        # extracts counter so it would be ignored
        if counter is None and line.replace(' ', '').upper().startswith("DO"):
            split_list = line.split('=')[0].split(' ')
            split_list = filter(None, split_list)  # removes empty str
            counter = split_list[len(split_list) - 1]
            continue

        # ignores comments and def's and empty strings
        ignore = False
        for keyWord in ignore_list:
            if not line or line.upper().startswith(keyWord):
                ignore = True
        if ignore:
            continue

        result = _get_var_list(line)

        # processes RHS
        var_list = result[0]
        for var in var_list:

            # ignores hardcoded digits and counter
            if var[0].isdigit():
                continue

            # checks arrays for memory loads
            if '(' in var and ')' in var:
                indexes = var[var.find("(")+1:var.find(")")]  # gets array indexes
                var = var.split('(')[0]  # gets array name

                if var not in loaded_arrays:
                    memory_load_count += 1
                    loaded_arrays.append(var)

                index_list = indexes.replace(' ', '').split(',')  # in case its multi dimensional array
                _process_indexes(index_list)

            else:  # checks scalars for memory loads

                while var.endswith(')'):  # removes any extra brackets if some where missed when processing
                    var = var.replace(')', '')

                if var != counter and var not in loaded_scalars:
                    memory_load_count += 1
                    loaded_scalars.append(var)

        # processes LHS
        if result[1]:

            lhs_var = result[1]
            if '(' in lhs_var and ')' in lhs_var:
                indexes = lhs_var[lhs_var.find("(")+1:lhs_var.find(")")]  # gets array indexes
                lhs_var = lhs_var.split('(')[0]  # gets array name

                if lhs_var not in loaded_arrays:
                    loaded_arrays.append(lhs_var)

                index_list = indexes.replace(' ', '').split(',')  # in case its multi dimensional array
                _process_indexes(index_list)

            else:  # checks scalars for memory loads
                if lhs_var not in loaded_scalars:
                    loaded_scalars.append(lhs_var)

    return memory_load_count
f = open(sys.argv[1], "rt")
get_memory_loads(f)




